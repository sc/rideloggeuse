""" Example for using the SGP30 with CircuitPython and the Adafruit library"""
 
import time
import board
import busio
import adafruit_sgp30
import datetime

time.sleep(10)
for line in open("/home/pi/rideloggeuse/baseline.csv",'rb'): pass #read last stored baseline
base=str(line.decode("utf-8")).rstrip().split(',')

print("last baseline eCO2:",base[0],",TVOC:",base[1])
 
i2c = busio.I2C(board.SCL, board.SDA, frequency=100000)
 
# Create library object on our I2C port
sgp30 = adafruit_sgp30.Adafruit_SGP30(i2c)
print("SGP30 serial #", [hex(i) for i in sgp30.serial])
sgp30.iaq_init()
sgp30.set_iaq_baseline(int(base[0],16),int(base[1],16))
elapsed_sec=0

output=open("/home/pi/data/air.csv","a") 
output.write("date, eCO2, TVOC\n")
output.close()
while True:
    output=open("/home/pi/data/air.csv","a") 
    output.write("%s,%d,%d\n" % (datetime.datetime.now().strftime("%Y%m%d%H%M%S"),sgp30.eCO2, sgp30.TVOC)) #write date and sensor values
    output.close()
    time.sleep(1)
    elapsed_sec+=1
    if elapsed_sec>100: #update baseline every hundred seconds
        elapsed_sec=0
        with open("/home/pi/rideloggeuse/baseline.csv", "a") as baseline: 
            baseline.write("0x%x,0x%x\n" % (sgp30.baseline_eCO2, sgp30.baseline_TVOC))
