#this is the path to the folder that contain the script of the repository
FOLDERINSTALL=/home/pi/rideloggeuse

#make all files executable
chmod a+x $FOLDERINSTALL/monitortmp.sh
chmod a+x $FOLDERINSTALL/monitortmpd
chmod a+x $FOLDERINSTALL/gps.sh
chmod a+x $FOLDERINSTALL/gpsd
chmod a+x $FOLDERINSTALL/aircontrold

#symbolic link
ln -s $FOLDERINSTALL/monitortmpd /etc/init.d/monitortmpd
ln -s $FOLDERINSTALL/monitortmp.sh /usr/bin/monitortmp.sh 
ln -s $FOLDERINSTALL/gpsd /etc/init.d/gpsd
ln -s $FOLDERINSTALL/gps.sh /usr/bin/gps 
ln -s $FOLDERINSTALL/aircontrold /etc/init.d/aircontrold

#record and enable the daemon
systemctl daemon-reload
systemctl enable monitortmpd
systemctl enable aircontrold 
systemctl enable gpsd 
