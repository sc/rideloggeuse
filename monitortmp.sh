DATA_FOLDER=/home/pi/data

moist1=16

echo $moist1 >  /sys/class/gpio/export
sleep 1
echo in > /sys/class/gpio/gpio$moist1/direction

pigpiod


if [ ! -d $DATA_FOLDER ]; then
	mkdir -p $DATA_FOLDER
fi


echo "start monitoring"

while true ; do
	alltemp=""
	for w1 in 28-00000a* ; do 
		u=$(cat /sys/devices/w1_bus_master1/$w1/w1_slave | grep t= | sed "s/t=//g" | awk '{print $10}')
		alltemp=$alltemp","$u","
 	done
	tdate=$(date -d "today" +"%Y%m%d%H%M%S")
	h1=$(/home/pi/fieldsound-beta/DHTXXD -g16 | awk '{print $3}')
	echo $tdate","$alltemp","$h1 >> $DATA_FOLDER/temperature.csv
	sleep 1
done


